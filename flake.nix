{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    crane.url = "github:ipetkov/crane";
    crane.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    rcodesign = {
      url = "github:indygreg/PyOxidizer";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, crane, flake-utils, rcodesign, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let 
        pkgs = import nixpkgs {
          inherit system;
        };
      in {
      packages.default = crane.lib.${system}.buildPackage {
        src = "${rcodesign}";
        doCheck = false;
        nativeBuildInputs = [ pkgs.python3 ];
      };
    });
}
